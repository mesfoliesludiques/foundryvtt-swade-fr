
/************************************************************************************/
Hooks.once('init', () => {
  if(typeof Babele !== 'undefined') {
		
    console.log("BABELE LOADED !!!");
		Babele.get().register({
			module: 'swade-fr',
			lang: 'fr',
			dir: 'compendiums'
		});
  }
  
});
